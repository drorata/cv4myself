% Author: Dror Atariah (drorata@gmail.com)
% A package to typeset my CV.
% The code is based on the source of the CV of T. P. Pavlic:
% http://www.tedpavlic.com/post_resume_cv_latex_example.php
%
% ----------------------------------------------------------------------%
% The following is copyright and licensing information for
% redistribution of this LaTeX source code; it also includes a liability
% statement. If this source code is not being redistributed to others,
% it may be omitted. It has no effect on the function of the above code.
% ----------------------------------------------------------------------%
%
% Unless otherwise expressly stated, this work is licensed under the
% Creative Commons Attribution-Noncommercial 3.0 United States License. To
% view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc/3.0/us/ or send a letter to
% Creative Commons, 171 Second Street, Suite 300, San Francisco,
% California, 94105, USA.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
% IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
% CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
% TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
% ----------------------------------------------------------------------%

\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{cv4myself}
[2011/01/11 v0.01 LaTeX package for my own purpose]

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{kvoptions}
\SetupKeyvalOptions{
  family=CFM,
  prefix=CFM@
}

\DeclareStringOption[a4]{page}[a4]
\DeclareStringOption[fullnumber]{pagenumber}[fullnumber]
\DeclareBoolOption[false]{git}
\DeclareBoolOption[false]{print}
\ProcessKeyvalOptions*
%%%%%%%%%%%%%%%%%%%%%%%%%%%% END Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%% Packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is a helpful package that puts math inside length specifications
\RequirePackage{ifthen}
\RequirePackage{etoolbox}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{calc}
\RequirePackage{xcolor}
%% This gives us fun enumeration environments. compactitem will be nice.
\RequirePackage{paralist}
\RequirePackage{tabto}
% Provides special list environments and macros to create new ones
\RequirePackage[shortlabels]{enumitem}
\RequirePackage{marvosym}
\RequirePackage{arevmath}
\RequirePackage{doi}
\RequirePackage{tikz,graphicx}
\usetikzlibrary{calc}
\RequirePackage{fancyhdr,lastpage}
% Finally, give us PDF bookmarks
\RequirePackage{hyperref}
\definecolor{darkblue}{rgb}{0.0,0.0,0.3}
\ifCFM@print
\hypersetup{colorlinks,breaklinks,
  linkcolor=black,urlcolor=black,
  anchorcolor=black,citecolor=black}
\else
\hypersetup{colorlinks,breaklinks,
  linkcolor=darkblue,urlcolor=darkblue,
  anchorcolor=darkblue,citecolor=darkblue}
\fi
%%%%%%%%%%%%%%%%%%%%%%%%%%%% End Packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%% Layout  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Layout: Puts the section titles on left side of page
\reversemarginpar

\ifthenelse{\equal{\CFM@page}{letter}}
{ \RequirePackage[paper=letterpaper,
  % includefoot, % Uncomment to put page number above margin
  marginparwidth=1.2in,     % Length of section titles
  marginparsep=.05in,       % Space between titles and text
  margin=1in,               % 1 inch margins
  includemp]{geometry}%
}
{ % Use a4paper by default.
  \RequirePackage[paper=a4paper,
  % includefoot, % Uncomment to put page number above margin
  marginparwidth=30.5mm,    % Length of section titles
  marginparsep=1.5mm,       % Space between titles and text
  margin=25mm,              % 25mm margins
  includemp]{geometry}%
}


% Obtain git information. Needs the shell-escape option.
\ifCFM@git
\immediate\write18{git log -1 --pretty=format:"\@percentchar h" > \jobname.githash}
\immediate\write18{git rev-parse --abbrev-ref HEAD > \jobname.gitbranch}
\fi

%% More layout: Get rid of indenting throughout entire document
\setlength{\parindent}{0in}

%% Setting the footer with page numbers optionally
\pagestyle{fancy}
\ifthenelse{\equal{\CFM@pagenumber}{nonumber}}
{
  % No page numbers
  \pagestyle{empty}
}
{\ifthenelse{\equal{\CFM@pagenumber}{withpagenumbers}}
{
  \fancyhf{}\renewcommand{\headrulewidth}{0pt}
  \fancyfootoffset{\marginparsep+\marginparwidth}
  \newlength{\footpageshift}
  \setlength{\footpageshift}
  {0.5\textwidth+0.5\marginparsep+0.5\marginparwidth-2in}
  \lfoot{\hspace{\footpageshift}%
  \parbox{4in}{\, \hfill %
  \arabic{page}
  \hfill \,}}
}
{
  % With full page numbers "Page k out of l"
  \fancyhf{}\renewcommand{\headrulewidth}{0pt}
  \fancyfootoffset{\marginparsep+\marginparwidth}
  \newlength{\footpageshift}
  \setlength{\footpageshift}
  {0.5\textwidth+0.5\marginparsep+0.5\marginparwidth-2in}
  \lfoot{\hspace{\footpageshift}%
  \parbox{4in}{\, \hfill %
  \arabic{page} of \protect\pageref*{LastPage}
  \hfill \,}}
}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%% End Layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%% Bib Lists %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simpler bibsections for CV sections
% (thanks to natbib for inspiration)
%
% * For lists of references with hanging indents and no numbers:
%
% \begin{bibsection}
% \item ...
% \end{bibsection}
%
% * For numbered lists of references (with hanging indents):
%
% \begin{bibenum}
% \item ...
% \end{bibenum}
%
% Note that bibenum numbers continuously throughout. To reset the
% counter, use
%
% \restartlist{bibenum}
%
% at the place where you want the numbering to reset.
\newlength{\bibhang}
\setlength{\bibhang}{1em}
\newlength{\bibsep}
{\@listi \global\bibsep\itemsep \global\advance\bibsep by\parsep}
\newlist{bibsection}{itemize}{3}
\setlist[bibsection]{label=,leftmargin=\bibhang,%
  itemindent=-\bibhang,
  itemsep=\bibsep,parsep=\z@,partopsep=0pt,
  topsep=0pt}
\newlist{bibenum}{enumerate}{3}
\setlist[bibenum]{label=[\arabic*],resume,leftmargin={\bibhang+\widthof{[999]}},%
  itemindent=-\bibhang,
  itemsep=\bibsep,parsep=\z@,partopsep=0pt,
  topsep=0pt}
\let\oldendbibenum\endbibenum
\def\endbibenum{\oldendbibenum\vspace{-.6\baselineskip}}
\let\oldendbibsection\endbibsection
\def\endbibsection{\oldendbibsection\vspace{-.6\baselineskip}}
%%%%%%%%%%%%%%%%%%%%%%%% End Bib List%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%% Heading maker %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Name and picture
% The title (name) with a horizontal rule under it
%
% usage: \name{<firstname>}{<lastname>}
\newcommand*{\name}[2]{\def\@firstname{#1}\def\@lastname{#2}}

% usage: \yourpicture{<filename-of-picture>} should be jpg!
\newcommand*{\yourpicture}[2]{\def\@yourpicture{#1}%
\def\@yourpicturewidth{#2}}
\newlength{\ProfilePich} % Will store the height of the profile picture
\newcommand{\setProfilePich}[1]{
  \settoheight{\ProfilePich}{\usebox{#1}}}
\newlength{\ProfilePicd} % Will store the depth  of the profile picture
\newcommand{\setProfilePicd}[1]{
  \settowidth{\ProfilePicd}{\usebox{#1}}}
\newlength{\ProfilePicw} % Will store the width  of the profile picture
\newcommand{\setProfilePicw}[1]{
  \settowidth{\ProfilePicw}{\usebox{#1}}}
\newsavebox{\ProfilePic}
\newcommand{\profilePicDimensions}[1]{
  % create and save the box
  \savebox{\ProfilePic}{\includegraphics[width=\@yourpicturewidth]{#1}}
  % \usebox{\ProfilePic}\
  \setProfilePich{\ProfilePic}
  \setProfilePicw{\ProfilePic}
  \setProfilePicd{\ProfilePic}}%

%
% Place at top of document. It should be the first thing.
\newcommand{\makeheading}%
{\hspace*{-\marginparsep minus \marginparwidth}%
  \begin{minipage}[t]{\textwidth+\marginparwidth+\marginparsep}%
    \begin{minipage}[t]{0.75\textwidth}%
      {\textsf{\Huge {\bfseries{\color{black!100}\@firstname}~{\@lastname}}}}
      \ifCFM@git
      ~--- \texttt{git:}
      \#\texttt{\input{\jobname.githash}} on branch: \texttt{\input{\jobname.gitbranch}}
      \fi
    \end{minipage}%
    \hfill%
    \@ifundefined{@yourpicture}%
    { }% No picture will be used
    {%
      % obtain the dimensions of the profile picture
      \profilePicDimensions{\@yourpicture}
      \begin{minipage}[t]{8pt+\@yourpicturewidth}%
        \begin{center}
          \begin{tikzpicture}
            \fill[black!20] ($(0,0)$) %
            node[anchor=south west] {\includegraphics[%
              keepaspectratio=true,width=\@yourpicturewidth]{\@yourpicture}} %
            rectangle ($(\the\ProfilePicw,\the\ProfilePich)$);
          \end{tikzpicture}
        \end{center}
      \end{minipage}
    }%
    \\[-0.5\baselineskip]%
    \rule{\columnwidth}{1pt}%
  \end{minipage}}
%%%%%%%%%%%%%%%%%%%%%%%%%%% END Heading maker %%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%% EXTRA SPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To add some paragraph space between lines.
% This also tells LaTeX to preferably break a page on one of these gaps
% if there is a needed pagebreak nearby.
\newcommand{\blankline}{\quad\pagebreak[3]}
\newcommand{\halfblankline}{\quad\vspace{-0.5\baselineskip}\pagebreak[3]}
%%%%%%%%%%%%%%%%%%%%%%%%%%% END EXTRA SPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%% Sections %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The section headings. Flush left in small caps down pseudo-margin.
%
% Usage: \section{section name}
\renewcommand{\section}[1]{\pagebreak[3]%
  % \vspace{1.8\baselineskip}%
  \vspace{1.3\baselineskip}%
  \phantomsection\addcontentsline{toc}{section}{#1}%
 \noindent\llap{%
   \bfseries\scshape\smash{%
     \parbox[t]{\marginparwidth}{\hyphenpenalty=10000\raggedright #1%
        % \\[-0.75\baselineskip]\rule{0.75\marginparwidth}{1pt}
      }}}%
  \vspace{-\baselineskip}\par}

% Typeset an entry in a chronological list.
% Usage: \entry{Title}{Sub title}{Date}
% Typically, should be \entry{Dreamworld Inc.}{Jounior researcher}{October 2034}
\newcommand*{\entry}[3]{%
  {\color{darkblue}\textbf{#1}}~#2 \hfill #3\\*[0.15\baselineskip]}
%%%%%%%%%%%%%%%%%%%%%%%%%%% END Sections %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%% Lists %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This macro alters a list by removing some of the space that follows the list
% (is used by lists below)
\newcommand*\fixendlist[1]{%
  \expandafter\let\csname preFixEndListend#1\expandafter\endcsname\csname end#1\endcsname
  \expandafter\def\csname end#1\endcsname{\csname preFixEndListend#1\endcsname\vspace{-0.6\baselineskip}}}

% These macros help ensure that items in outer-type lists do not get
% separated from the next line by a page break
% (they are used by lists below)
\let\originalItem\item
\newcommand*\fixouterlist[1]{%
  \expandafter\let\csname preFixOuterList#1\expandafter\endcsname\csname #1\endcsname
  \expandafter\def\csname #1\endcsname{\let\oldItem\item\def\item{\pagebreak[2]\oldItem}\csname preFixOuterList#1\endcsname}
  \expandafter\let\csname preFixOuterListend#1\expandafter\endcsname\csname end#1\endcsname
  \expandafter\def\csname end#1\endcsname{\let\item\oldItem\csname preFixOuterListend#1\endcsname}}
\newcommand*\fixinnerlist[1]{%
  \expandafter\let\csname preFixInnerList#1\expandafter\endcsname\csname #1\endcsname
  \expandafter\def\csname #1\endcsname{\let\oldItem\item\let\item\originalItem\csname preFixInnerList#1\endcsname}
  \expandafter\let\csname preFixInnerListend#1\expandafter\endcsname\csname end#1\endcsname
  \expandafter\def\csname end#1\endcsname{\csname preFixInnerListend#1\endcsname\let\item\oldItem}}

% An itemize-style list with lots of space between items
%
% Usage:
% \begin{outerlist}
% \item ...    % (or \item[] for no bullet)
% \end{outerlist}
\newlist{outerlist}{itemize}{3}
\setlist[outerlist]{label=\enskip\textbullet,leftmargin=*}
\fixendlist{outerlist}
\fixouterlist{outerlist}

% An environment IDENTICAL to outerlist that has better pre-list spacing
% when used as the first thing in a \section
%
% Usage:
% \begin{lonelist}
% \item ...    % (or \item[] for no bullet)
% \end{lonelist}
\newlist{lonelist}{itemize}{3}
\setlist[lonelist]{label=\enskip\textbullet,leftmargin=*,partopsep=0pt,topsep=0pt}
\fixendlist{lonelist}
\fixouterlist{lonelist}

% An itemize-style list with little space between items
%
% Usage:
% \begin{innerlist}
% \item ...    % (or \item[] for no bullet)
% \end{innerlist}
\newlist{innerlist}{itemize}{3}
\setlist[innerlist]{label=\enskip\textbullet,leftmargin=*,parsep=0pt,itemsep=0pt,topsep=0pt,partopsep=0pt}
\fixinnerlist{innerlist}

% An environment IDENTICAL to innerlist that has better pre-list spacing
% when used as the first thing in a \section
%
% Usage:
% \begin{loneinnerlist}
% \item ...    % (or \item[] for no bullet)
% \end{loneinnerlist}
\newlist{loneinnerlist}{itemize}{3}
\setlist[loneinnerlist]{label=\enskip\textbullet,leftmargin=*,parsep=0pt,itemsep=0pt,topsep=0pt,partopsep=0pt}
\fixendlist{loneinnerlist}
\fixinnerlist{loneinnerlist}

% Inline tabbed list
\newenvironment{tabbedenum}[1]
{\NumTabs{#1}\inparaenum\let\latexitem\item
  \def\item{%
    \def\item{\tab\latexitem}\latexitem}}
{\endinparaenum}

%%%%%%%%%%%%%%%%%%%%%%%% END Lists %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\endinput